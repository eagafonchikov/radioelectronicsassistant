﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkingSigns
{
    class ResistorFactorStrip
    {
        private static Dictionary<float, string> factorDict;
        private static Dictionary<int, Brush> factorStrip;

        public static Dictionary<float, string> getValues()
        {
            factorDict = new Dictionary<float, string>();

            factorDict.Add(0.01f, "Серебристый");
            factorDict.Add(0.1f, "Золотистый");
            factorDict.Add(1.0f, "Чёрный");
            factorDict.Add(10.0f, "Коричневый");
            factorDict.Add(100.0f, "Красный");
            factorDict.Add(1000.0f, "Оранжевый");
            factorDict.Add(10000.0f, "Жёлтый");
            factorDict.Add(100000.0f, "Зелёный");
            factorDict.Add(1000000.0f, "Синий");
            factorDict.Add(10000000.0f, "Фиолетовый");
            factorDict.Add(100000000.0f, "Серый");
            factorDict.Add(1000000000.0f, "Белый");

            return factorDict;
        }

        public static Dictionary<int, Brush> getColors()
        {
            factorStrip = new Dictionary<int, Brush>();

            factorStrip.Add(0, Brushes.Silver);
            factorStrip.Add(1, Brushes.Gold);
            factorStrip.Add(2, Brushes.Black);
            factorStrip.Add(3, Brushes.Brown);
            factorStrip.Add(4, Brushes.Red);
            factorStrip.Add(5, Brushes.Orange);
            factorStrip.Add(6, Brushes.Yellow);
            factorStrip.Add(7, Brushes.LawnGreen);
            factorStrip.Add(8, Brushes.Blue);
            factorStrip.Add(9, Brushes.Purple);
            factorStrip.Add(10, Brushes.DarkGray);
            factorStrip.Add(11, Brushes.White);

            return factorStrip;
        }
    }
}
