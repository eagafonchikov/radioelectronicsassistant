﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkingSigns
{
    class ResistorErrorStrip
    {
        private static Dictionary<float, string> errorDict;
        private static Dictionary<int, Brush> errorStrip;

        public static Dictionary<float, string> getValues()
        {
            errorDict = new Dictionary<float, string>();

            errorDict.Add(0.05f, "Серый");
            errorDict.Add(0.1f, "Фиолетовый");
            errorDict.Add(0.25f, "Синий");
            errorDict.Add(0.5f, "Зелёный");
            errorDict.Add(1.0f, "Коричневый");
            errorDict.Add(2.0f, "Красный");
            errorDict.Add(5.0f, "Золотистый");
            errorDict.Add(10.0f, "Серебристый");

            return errorDict;
        }

        public static Dictionary<int, Brush> getColors()
        {
            errorStrip = new Dictionary<int, Brush>();

            errorStrip.Add(0, Brushes.DarkGray);
            errorStrip.Add(1, Brushes.Purple);
            errorStrip.Add(2, Brushes.Blue);
            errorStrip.Add(3, Brushes.LawnGreen);
            errorStrip.Add(4, Brushes.Brown);
            errorStrip.Add(5, Brushes.Red);
            errorStrip.Add(6, Brushes.Gold);
            errorStrip.Add(7, Brushes.Silver);

            return errorStrip;
        }
    }
}
