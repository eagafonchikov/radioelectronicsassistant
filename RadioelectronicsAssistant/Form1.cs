﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkingSigns
{
    public partial class Form1 : Form
    {
        Dictionary<int, string> valueDict;
        Dictionary<float, string> factorDict;
        Dictionary<float, string> errorDict;

        Dictionary<int, Brush> valueStrip;
        Dictionary<int, Brush> factorStrip;
        Dictionary<int, Brush> errorStrip;

        ToolTip helpToolTip;

        public Form1()
        {
            InitializeComponent();

            valueDict = ResistorValueStrip.getValues();
            valueStrip = ResistorValueStrip.getColors();
            factorDict = ResistorFactorStrip.getValues();
            factorStrip = ResistorFactorStrip.getColors();
            errorDict = ResistorErrorStrip.getValues();
            errorStrip = ResistorErrorStrip.getColors();

            Resistor.CreateResistorValueControl(firstStripComboBox, valueDict);
            Resistor.CreateResistorValueControl(secondStripComboBox, valueDict);
            Resistor.CreateResistorValueControl(thirdStripComboBox, valueDict);
            Resistor.CreateResistorFloatControl(factorStripComboBox, factorDict);
            Resistor.CreateResistorFloatControl(errorComboBox, errorDict);

            helpToolTip = HelpProvider.Init();
            SetResistorHelp();
        }

        void SetResistorHelp()
        {
            helpToolTip.SetToolTip(resistorHelpLabel, HelpProvider.GetResistorHelpString());
            helpToolTip.SetToolTip(firstStripComboBox, "1-я полоса 4-хполосного");
            helpToolTip.SetToolTip(secondStripComboBox, "1-я полоса 3-хполосного или 2-я 4-хполосного");
            helpToolTip.SetToolTip(thirdStripComboBox, "2-я полоса 3-хполосного или 3-я 4-хполосного");
            helpToolTip.SetToolTip(factorStripComboBox, "3-я полоса 3-хполосного или 4-я 4-хполосного");
            helpToolTip.SetToolTip(errorComboBox, "Полоса погрешности");
            helpToolTip.SetToolTip(valueTextBox, "Номинал");
            helpToolTip.SetToolTip(errorTextBox, "Погрешность");
        }

        void SetCapacitorHelp()
        {
            helpToolTip.SetToolTip(capacitorHelpLabel, HelpProvider.GetCapacitorHelpString());
            helpToolTip.SetToolTip(valueCapTextBox, "Трёхзначное число с конденсатора");
        }

        void SetFreqFilterHelp()
        {
            helpToolTip.SetToolTip(freqFilterHelpLabel, HelpProvider.GetFrequencyFilterHelp());
        }

        void SetLedRresistorHelp()
        {
            helpToolTip.SetToolTip(ledResistorHelpLabel, HelpProvider.GetLedResistorHelp());
            helpToolTip.SetToolTip(ledResultTextBox, "Резистор такого номинала нужно подключить перед светодиодом");
            helpToolTip.SetToolTip(ledSourceVoltageTextBox, "Напряжение источника питания цепи");
            helpToolTip.SetToolTip(ledNominalVoltageTextBox, "Номинальное напряжение светодиода");
            helpToolTip.SetToolTip(ledCurrentTextBox, "Ток через светодиод");
        }

        void SetResistorResult()
        {
            valueTextBox.Text = Resistor.CountResistorResult(firstStripComboBox.SelectedValue,
                secondStripComboBox.SelectedValue, thirdStripComboBox.SelectedValue,
                factorStripComboBox.SelectedValue, fourRadioButton.Checked) + " Ом";
        }

        #region EventHandlers

        private void firstStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetResistorResult();
            firstStripComboBox.BackColor = ((SolidBrush)(valueStrip[firstStripComboBox.SelectedIndex])).Color;
        }

        private void secondStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetResistorResult();
            secondStripComboBox.BackColor = ((SolidBrush)(valueStrip[secondStripComboBox.SelectedIndex])).Color;
        }

        private void thirdStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetResistorResult();
            thirdStripComboBox.BackColor = ((SolidBrush)(valueStrip[thirdStripComboBox.SelectedIndex])).Color;
        }

        private void factorStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetResistorResult();
            factorStripComboBox.BackColor = ((SolidBrush)(factorStrip[factorStripComboBox.SelectedIndex])).Color;
        }

        private void errorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorTextBox.Text = "± " + errorComboBox.SelectedValue.ToString() + "%";
            errorComboBox.BackColor = ((SolidBrush)(errorStrip[errorComboBox.SelectedIndex])).Color;
        }

        private void threeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            firstStripComboBox.Visible = false;
            secondStripComboBox.Visible = true;
            thirdStripComboBox.Visible = true;
            factorStripComboBox.Visible = true;
            errorComboBox.Visible = true;

            resistorPreTextBox.Visible = false;
            resistorFactorLetterTextBox.Visible = false;
            resistorAfterPointTextBox.Visible = false;
            resistorErrorTextBox.Visible = false;
            SetResistorResult();
        }

        private void fourRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            firstStripComboBox.Visible = true;
            secondStripComboBox.Visible = true;
            thirdStripComboBox.Visible = true;
            factorStripComboBox.Visible = true;
            errorComboBox.Visible = true;

            resistorPreTextBox.Visible = false;
            resistorFactorLetterTextBox.Visible = false;
            resistorAfterPointTextBox.Visible = false;
            resistorErrorTextBox.Visible = false;
            SetResistorResult();
        }

        private void firstStripComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            Drawer.DrawItem((ComboBox)sender, e, valueDict, valueStrip);
        }

        private void secondStripComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            Drawer.DrawItem((ComboBox)sender, e, valueDict, valueStrip);
        }

        private void thirdStripComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            Drawer.DrawItem((ComboBox)sender, e, valueDict, valueStrip);
        }

        private void factorStripComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            Drawer.DrawFloatItem((ComboBox)sender, e, factorStrip);
        }

        private void errorComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            Drawer.DrawFloatItem((ComboBox)sender, e, errorStrip);
        }

        private void valueCapTextBox_TextChanged(object sender, EventArgs e)
        {
            double input;
            double.TryParse(valueCapTextBox.Text, out input);
            double result = ((input - (input % 10)) / 10) * Math.Pow(10, input % 10);
            pFTextBox.Text = result.ToString() + " pF";
            nFTextBox.Text = (result * Math.Pow(10, -3)).ToString() + " nF";
            mFTextBox.Text = (result * Math.Pow(10, -6)).ToString() + " µF";
        }

        private void freqResistorTextBox_TextChanged(object sender, EventArgs e)
        {
            if (freqRadioButton.Checked)
            {
                freqTextBox.Text = Frequency.CalculateFrequencyValues(freqResistorTextBox.Text,
                    freqCapacitorTextBox.Text);
            }
            else if (freqCapacitorRadioButton.Checked)
            {
                freqCapacitorTextBox.Text = Frequency.CalculateFrequencyValues(freqTextBox.Text,
                    freqResistorTextBox.Text);
            }
        }

        private void freqCapacitorTextBox_TextChanged(object sender, EventArgs e)
        {
            if (freqRadioButton.Checked)
            {
                freqTextBox.Text = Frequency.CalculateFrequencyValues(freqResistorTextBox.Text,
                    freqCapacitorTextBox.Text);
            }
            else if (freqResistorRadioButton.Checked)
            {
                freqResistorTextBox.Text = Frequency.CalculateFrequencyValues(freqTextBox.Text,
                    freqCapacitorTextBox.Text);
            }
        }

        private void freqTextBox_TextChanged(object sender, EventArgs e)
        {
            if (freqCapacitorRadioButton.Checked)
            {
                freqCapacitorTextBox.Text = Frequency.CalculateFrequencyValues(freqResistorTextBox.Text, 
                    freqTextBox.Text);
            }
            else if (freqResistorRadioButton.Checked)
            {
                freqResistorTextBox.Text = Frequency.CalculateFrequencyValues(freqTextBox.Text,
                    freqCapacitorTextBox.Text);
            }
        }

        private void ledSourceVoltageTextBox_TextChanged(object sender, EventArgs e)
        {
            ledResultTextBox.Text = Resistor.CalculateLedResistor(ledSourceVoltageTextBox.Text,
                ledNominalVoltageTextBox.Text, ledCurrentTextBox.Text);
        }

        private void ledNominalVoltageTextBox_TextChanged(object sender, EventArgs e)
        {
            ledResultTextBox.Text = Resistor.CalculateLedResistor(ledSourceVoltageTextBox.Text,
                ledNominalVoltageTextBox.Text, ledCurrentTextBox.Text);
        }

        private void ledCurrentTextBox_TextChanged(object sender, EventArgs e)
        {
            ledResultTextBox.Text = Resistor.CalculateLedResistor(ledSourceVoltageTextBox.Text,
                ledNominalVoltageTextBox.Text, ledCurrentTextBox.Text);
        }
#endregion

        private void textRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            firstStripComboBox.Visible = false;
            secondStripComboBox.Visible = false;
            thirdStripComboBox.Visible = false;
            factorStripComboBox.Visible = false;
            errorComboBox.Visible = false;

            resistorPreTextBox.Visible = true;
            resistorFactorLetterTextBox.Visible = true;
            resistorAfterPointTextBox.Visible = true;
            resistorErrorTextBox.Visible = true;

            SetTextResistorResult();
        }

        private void SetTextResistorResult()
        {
            valueTextBox.Text = Resistor.CountResistorResult(resistorPreTextBox.Text,
                resistorFactorLetterTextBox.Text, resistorAfterPointTextBox.Text) + " Ом";
            errorTextBox.Text = "± " + Resistor.GetErrorByLetter(resistorErrorTextBox.Text).ToString() + "%";
        }

        private void resistorPreTextBox_TextChanged(object sender, EventArgs e)
        {
            SetTextResistorResult();
        }

        private void resistorFactorLetterTextBox_TextChanged(object sender, EventArgs e)
        {
            SetTextResistorResult();
        }

        private void resistorAfterPointTextBox_TextChanged(object sender, EventArgs e)
        {
            SetTextResistorResult();
        }

        private void resistorErrorTextBox_TextChanged(object sender, EventArgs e)
        {
            SetTextResistorResult();
        }

        private void itemTabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            String tab = e.TabPage.Name;
            switch (tab)
            {
                case "resistorPage":
                    SetResistorHelp();
                    break;
                case "capacitorsPage":
                    SetCapacitorHelp();
                    break;
                case "filtersTabPage":
                    SetFreqFilterHelp();
                    break;
                case "resistorLedTabPage":
                    SetLedRresistorHelp();
                    break;
                default:
                    break;
            }
        }
    }
}
