﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkingSigns
{
    public class ResistorValueStrip
    {
        private static Dictionary<int, string> valueDict;
        private static Dictionary<int, Brush> valueStrip;

        public static Dictionary<int, string> getValues()
        {
            valueDict = new Dictionary<int, string>();

            valueDict.Add(0, "Чёрный");
            valueDict.Add(1, "Коричневый");
            valueDict.Add(2, "Красный");
            valueDict.Add(3, "Оранжевый");
            valueDict.Add(4, "Жёлтый");
            valueDict.Add(5, "Зелёный");
            valueDict.Add(6, "Синий");
            valueDict.Add(7, "Фиолетовый");

            return valueDict;
        }

        public static Dictionary<int, Brush> getColors()
        {
            valueStrip = new Dictionary<int, Brush>();

            valueStrip.Add(0, Brushes.Black);
            valueStrip.Add(1, Brushes.Brown);
            valueStrip.Add(2, Brushes.Red);
            valueStrip.Add(3, Brushes.Orange);
            valueStrip.Add(4, Brushes.Yellow);
            valueStrip.Add(5, Brushes.LawnGreen);
            valueStrip.Add(6, Brushes.Blue);
            valueStrip.Add(7, Brushes.Purple);

            return valueStrip;
        }
    }
}
