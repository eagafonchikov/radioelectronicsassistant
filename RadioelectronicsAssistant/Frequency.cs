﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkingSigns
{
    class Frequency
    {
        public static string CalculateFrequencyValues(string firstParameter, string secondParameter)
        {
            if (firstParameter != "" && secondParameter != "")
            {
                double firstValue, secondValue;
                double.TryParse(firstParameter, out firstValue);
                double.TryParse(secondParameter, out secondValue);
                if (firstValue != 0 && secondValue != 0)
                {
                    return (1 / (2 * Math.PI * firstValue * secondValue)).ToString("F99").TrimEnd("0".ToCharArray());
                }
            }
            return "";
        }
    }
}
