﻿namespace MarkingSigns
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemTabs = new System.Windows.Forms.TabControl();
            this.resistorPage = new System.Windows.Forms.TabPage();
            this.resistorHelpLabel = new System.Windows.Forms.Label();
            this.resistorErrorTextBox = new System.Windows.Forms.TextBox();
            this.resistorAfterPointTextBox = new System.Windows.Forms.TextBox();
            this.resistorFactorLetterTextBox = new System.Windows.Forms.TextBox();
            this.resistorPreTextBox = new System.Windows.Forms.TextBox();
            this.errorTextBox = new System.Windows.Forms.TextBox();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.textRadioButton = new System.Windows.Forms.RadioButton();
            this.fourRadioButton = new System.Windows.Forms.RadioButton();
            this.threeRadioButton = new System.Windows.Forms.RadioButton();
            this.errorComboBox = new System.Windows.Forms.ComboBox();
            this.factorStripComboBox = new System.Windows.Forms.ComboBox();
            this.thirdStripComboBox = new System.Windows.Forms.ComboBox();
            this.secondStripComboBox = new System.Windows.Forms.ComboBox();
            this.firstStripComboBox = new System.Windows.Forms.ComboBox();
            this.capacitorsPage = new System.Windows.Forms.TabPage();
            this.capacitorHelpLabel = new System.Windows.Forms.Label();
            this.nFTextBox = new System.Windows.Forms.TextBox();
            this.pFTextBox = new System.Windows.Forms.TextBox();
            this.mFTextBox = new System.Windows.Forms.TextBox();
            this.valueCapTextBox = new System.Windows.Forms.TextBox();
            this.filtersTabPage = new System.Windows.Forms.TabPage();
            this.freqFilterHelpLabel = new System.Windows.Forms.Label();
            this.freqTextBox = new System.Windows.Forms.TextBox();
            this.freqCapacitorTextBox = new System.Windows.Forms.TextBox();
            this.freqLabel = new System.Windows.Forms.Label();
            this.capacitorLabel = new System.Windows.Forms.Label();
            this.resistorLable = new System.Windows.Forms.Label();
            this.freqResistorTextBox = new System.Windows.Forms.TextBox();
            this.freqCapacitorRadioButton = new System.Windows.Forms.RadioButton();
            this.freqResistorRadioButton = new System.Windows.Forms.RadioButton();
            this.freqRadioButton = new System.Windows.Forms.RadioButton();
            this.resistorLedTabPage = new System.Windows.Forms.TabPage();
            this.ledResistorHelpLabel = new System.Windows.Forms.Label();
            this.ledResultTextBox = new System.Windows.Forms.TextBox();
            this.currentLabel = new System.Windows.Forms.Label();
            this.voltageLabel = new System.Windows.Forms.Label();
            this.sourceLabel = new System.Windows.Forms.Label();
            this.ledCurrentTextBox = new System.Windows.Forms.TextBox();
            this.ledNominalVoltageTextBox = new System.Windows.Forms.TextBox();
            this.ledSourceVoltageTextBox = new System.Windows.Forms.TextBox();
            this.itemTabs.SuspendLayout();
            this.resistorPage.SuspendLayout();
            this.capacitorsPage.SuspendLayout();
            this.filtersTabPage.SuspendLayout();
            this.resistorLedTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemTabs
            // 
            this.itemTabs.Controls.Add(this.resistorPage);
            this.itemTabs.Controls.Add(this.capacitorsPage);
            this.itemTabs.Controls.Add(this.filtersTabPage);
            this.itemTabs.Controls.Add(this.resistorLedTabPage);
            this.itemTabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.itemTabs.Location = new System.Drawing.Point(12, 2);
            this.itemTabs.Name = "itemTabs";
            this.itemTabs.SelectedIndex = 0;
            this.itemTabs.Size = new System.Drawing.Size(356, 401);
            this.itemTabs.TabIndex = 0;
            this.itemTabs.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.itemTabs_Selecting);
            // 
            // resistorPage
            // 
            this.resistorPage.Controls.Add(this.resistorHelpLabel);
            this.resistorPage.Controls.Add(this.resistorErrorTextBox);
            this.resistorPage.Controls.Add(this.resistorAfterPointTextBox);
            this.resistorPage.Controls.Add(this.resistorFactorLetterTextBox);
            this.resistorPage.Controls.Add(this.resistorPreTextBox);
            this.resistorPage.Controls.Add(this.errorTextBox);
            this.resistorPage.Controls.Add(this.valueTextBox);
            this.resistorPage.Controls.Add(this.textRadioButton);
            this.resistorPage.Controls.Add(this.fourRadioButton);
            this.resistorPage.Controls.Add(this.threeRadioButton);
            this.resistorPage.Controls.Add(this.errorComboBox);
            this.resistorPage.Controls.Add(this.factorStripComboBox);
            this.resistorPage.Controls.Add(this.thirdStripComboBox);
            this.resistorPage.Controls.Add(this.secondStripComboBox);
            this.resistorPage.Controls.Add(this.firstStripComboBox);
            this.resistorPage.Location = new System.Drawing.Point(4, 22);
            this.resistorPage.Name = "resistorPage";
            this.resistorPage.Padding = new System.Windows.Forms.Padding(3);
            this.resistorPage.Size = new System.Drawing.Size(348, 375);
            this.resistorPage.TabIndex = 0;
            this.resistorPage.Text = "Резисторы";
            this.resistorPage.UseVisualStyleBackColor = true;
            // 
            // resistorHelpLabel
            // 
            this.resistorHelpLabel.AutoSize = true;
            this.resistorHelpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resistorHelpLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.resistorHelpLabel.Location = new System.Drawing.Point(318, 343);
            this.resistorHelpLabel.Name = "resistorHelpLabel";
            this.resistorHelpLabel.Size = new System.Drawing.Size(25, 26);
            this.resistorHelpLabel.TabIndex = 14;
            this.resistorHelpLabel.Text = "?";
            // 
            // resistorErrorTextBox
            // 
            this.resistorErrorTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.resistorErrorTextBox.Location = new System.Drawing.Point(278, 41);
            this.resistorErrorTextBox.Name = "resistorErrorTextBox";
            this.resistorErrorTextBox.Size = new System.Drawing.Size(39, 20);
            this.resistorErrorTextBox.TabIndex = 13;
            this.resistorErrorTextBox.Text = "M";
            this.resistorErrorTextBox.Visible = false;
            this.resistorErrorTextBox.TextChanged += new System.EventHandler(this.resistorErrorTextBox_TextChanged);
            // 
            // resistorAfterPointTextBox
            // 
            this.resistorAfterPointTextBox.Location = new System.Drawing.Point(232, 41);
            this.resistorAfterPointTextBox.Name = "resistorAfterPointTextBox";
            this.resistorAfterPointTextBox.Size = new System.Drawing.Size(39, 20);
            this.resistorAfterPointTextBox.TabIndex = 12;
            this.resistorAfterPointTextBox.Text = "7";
            this.resistorAfterPointTextBox.Visible = false;
            this.resistorAfterPointTextBox.TextChanged += new System.EventHandler(this.resistorAfterPointTextBox_TextChanged);
            // 
            // resistorFactorLetterTextBox
            // 
            this.resistorFactorLetterTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.resistorFactorLetterTextBox.Location = new System.Drawing.Point(200, 41);
            this.resistorFactorLetterTextBox.Name = "resistorFactorLetterTextBox";
            this.resistorFactorLetterTextBox.Size = new System.Drawing.Size(26, 20);
            this.resistorFactorLetterTextBox.TabIndex = 11;
            this.resistorFactorLetterTextBox.Text = "R";
            this.resistorFactorLetterTextBox.Visible = false;
            this.resistorFactorLetterTextBox.TextChanged += new System.EventHandler(this.resistorFactorLetterTextBox_TextChanged);
            // 
            // resistorPreTextBox
            // 
            this.resistorPreTextBox.Location = new System.Drawing.Point(154, 41);
            this.resistorPreTextBox.Name = "resistorPreTextBox";
            this.resistorPreTextBox.Size = new System.Drawing.Size(40, 20);
            this.resistorPreTextBox.TabIndex = 10;
            this.resistorPreTextBox.Text = "4";
            this.resistorPreTextBox.Visible = false;
            this.resistorPreTextBox.TextChanged += new System.EventHandler(this.resistorPreTextBox_TextChanged);
            // 
            // errorTextBox
            // 
            this.errorTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.errorTextBox.Location = new System.Drawing.Point(217, 144);
            this.errorTextBox.Name = "errorTextBox";
            this.errorTextBox.ReadOnly = true;
            this.errorTextBox.Size = new System.Drawing.Size(100, 20);
            this.errorTextBox.TabIndex = 9;
            this.errorTextBox.Text = "Погрешность";
            // 
            // valueTextBox
            // 
            this.valueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueTextBox.Location = new System.Drawing.Point(217, 107);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.ReadOnly = true;
            this.valueTextBox.Size = new System.Drawing.Size(100, 20);
            this.valueTextBox.TabIndex = 8;
            this.valueTextBox.Text = "Значение";
            // 
            // textRadioButton
            // 
            this.textRadioButton.AutoSize = true;
            this.textRadioButton.Location = new System.Drawing.Point(43, 64);
            this.textRadioButton.Name = "textRadioButton";
            this.textRadioButton.Size = new System.Drawing.Size(86, 17);
            this.textRadioButton.TabIndex = 7;
            this.textRadioButton.TabStop = true;
            this.textRadioButton.Text = "Текст (тест)";
            this.textRadioButton.UseVisualStyleBackColor = true;
            this.textRadioButton.CheckedChanged += new System.EventHandler(this.textRadioButton_CheckedChanged);
            // 
            // fourRadioButton
            // 
            this.fourRadioButton.AutoSize = true;
            this.fourRadioButton.Location = new System.Drawing.Point(43, 41);
            this.fourRadioButton.Name = "fourRadioButton";
            this.fourRadioButton.Size = new System.Drawing.Size(74, 17);
            this.fourRadioButton.TabIndex = 6;
            this.fourRadioButton.Text = "4 Полосы";
            this.fourRadioButton.UseVisualStyleBackColor = true;
            this.fourRadioButton.CheckedChanged += new System.EventHandler(this.fourRadioButton_CheckedChanged);
            // 
            // threeRadioButton
            // 
            this.threeRadioButton.AutoSize = true;
            this.threeRadioButton.Checked = true;
            this.threeRadioButton.Location = new System.Drawing.Point(43, 18);
            this.threeRadioButton.Name = "threeRadioButton";
            this.threeRadioButton.Size = new System.Drawing.Size(74, 17);
            this.threeRadioButton.TabIndex = 5;
            this.threeRadioButton.TabStop = true;
            this.threeRadioButton.Text = "3 Полосы";
            this.threeRadioButton.UseVisualStyleBackColor = true;
            this.threeRadioButton.CheckedChanged += new System.EventHandler(this.threeRadioButton_CheckedChanged);
            // 
            // errorComboBox
            // 
            this.errorComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.errorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.errorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.errorComboBox.FormattingEnabled = true;
            this.errorComboBox.ItemHeight = 20;
            this.errorComboBox.Location = new System.Drawing.Point(62, 262);
            this.errorComboBox.Name = "errorComboBox";
            this.errorComboBox.Size = new System.Drawing.Size(121, 26);
            this.errorComboBox.TabIndex = 4;
            this.errorComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.errorComboBox_DrawItem);
            this.errorComboBox.SelectedIndexChanged += new System.EventHandler(this.errorComboBox_SelectedIndexChanged);
            // 
            // factorStripComboBox
            // 
            this.factorStripComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.factorStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.factorStripComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.factorStripComboBox.ForeColor = System.Drawing.Color.White;
            this.factorStripComboBox.FormattingEnabled = true;
            this.factorStripComboBox.ItemHeight = 20;
            this.factorStripComboBox.Location = new System.Drawing.Point(62, 219);
            this.factorStripComboBox.Name = "factorStripComboBox";
            this.factorStripComboBox.Size = new System.Drawing.Size(121, 26);
            this.factorStripComboBox.TabIndex = 3;
            this.factorStripComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.factorStripComboBox_DrawItem);
            this.factorStripComboBox.SelectedIndexChanged += new System.EventHandler(this.factorStripComboBox_SelectedIndexChanged);
            // 
            // thirdStripComboBox
            // 
            this.thirdStripComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.thirdStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.thirdStripComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.thirdStripComboBox.ForeColor = System.Drawing.Color.White;
            this.thirdStripComboBox.FormattingEnabled = true;
            this.thirdStripComboBox.ItemHeight = 20;
            this.thirdStripComboBox.Location = new System.Drawing.Point(62, 181);
            this.thirdStripComboBox.Name = "thirdStripComboBox";
            this.thirdStripComboBox.Size = new System.Drawing.Size(121, 26);
            this.thirdStripComboBox.TabIndex = 2;
            this.thirdStripComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.thirdStripComboBox_DrawItem);
            this.thirdStripComboBox.SelectedIndexChanged += new System.EventHandler(this.thirdStripComboBox_SelectedIndexChanged);
            // 
            // secondStripComboBox
            // 
            this.secondStripComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.secondStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.secondStripComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.secondStripComboBox.ForeColor = System.Drawing.Color.White;
            this.secondStripComboBox.FormattingEnabled = true;
            this.secondStripComboBox.ItemHeight = 20;
            this.secondStripComboBox.Location = new System.Drawing.Point(62, 144);
            this.secondStripComboBox.Name = "secondStripComboBox";
            this.secondStripComboBox.Size = new System.Drawing.Size(121, 26);
            this.secondStripComboBox.TabIndex = 1;
            this.secondStripComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.secondStripComboBox_DrawItem);
            this.secondStripComboBox.SelectedIndexChanged += new System.EventHandler(this.secondStripComboBox_SelectedIndexChanged);
            // 
            // firstStripComboBox
            // 
            this.firstStripComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.firstStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstStripComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.firstStripComboBox.ForeColor = System.Drawing.Color.White;
            this.firstStripComboBox.FormattingEnabled = true;
            this.firstStripComboBox.ItemHeight = 20;
            this.firstStripComboBox.Location = new System.Drawing.Point(62, 107);
            this.firstStripComboBox.Name = "firstStripComboBox";
            this.firstStripComboBox.Size = new System.Drawing.Size(121, 26);
            this.firstStripComboBox.TabIndex = 0;
            this.firstStripComboBox.Visible = false;
            this.firstStripComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.firstStripComboBox_DrawItem);
            this.firstStripComboBox.SelectedIndexChanged += new System.EventHandler(this.firstStripComboBox_SelectedIndexChanged);
            // 
            // capacitorsPage
            // 
            this.capacitorsPage.Controls.Add(this.capacitorHelpLabel);
            this.capacitorsPage.Controls.Add(this.nFTextBox);
            this.capacitorsPage.Controls.Add(this.pFTextBox);
            this.capacitorsPage.Controls.Add(this.mFTextBox);
            this.capacitorsPage.Controls.Add(this.valueCapTextBox);
            this.capacitorsPage.Location = new System.Drawing.Point(4, 22);
            this.capacitorsPage.Name = "capacitorsPage";
            this.capacitorsPage.Padding = new System.Windows.Forms.Padding(3);
            this.capacitorsPage.Size = new System.Drawing.Size(348, 375);
            this.capacitorsPage.TabIndex = 1;
            this.capacitorsPage.Text = "Конденсаторы";
            this.capacitorsPage.UseVisualStyleBackColor = true;
            // 
            // capacitorHelpLabel
            // 
            this.capacitorHelpLabel.AutoSize = true;
            this.capacitorHelpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.capacitorHelpLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.capacitorHelpLabel.Location = new System.Drawing.Point(317, 346);
            this.capacitorHelpLabel.Name = "capacitorHelpLabel";
            this.capacitorHelpLabel.Size = new System.Drawing.Size(25, 26);
            this.capacitorHelpLabel.TabIndex = 15;
            this.capacitorHelpLabel.Text = "?";
            // 
            // nFTextBox
            // 
            this.nFTextBox.Location = new System.Drawing.Point(219, 109);
            this.nFTextBox.Name = "nFTextBox";
            this.nFTextBox.ReadOnly = true;
            this.nFTextBox.Size = new System.Drawing.Size(100, 20);
            this.nFTextBox.TabIndex = 6;
            // 
            // pFTextBox
            // 
            this.pFTextBox.Location = new System.Drawing.Point(219, 77);
            this.pFTextBox.Name = "pFTextBox";
            this.pFTextBox.ReadOnly = true;
            this.pFTextBox.Size = new System.Drawing.Size(100, 20);
            this.pFTextBox.TabIndex = 5;
            // 
            // mFTextBox
            // 
            this.mFTextBox.Location = new System.Drawing.Point(219, 142);
            this.mFTextBox.Name = "mFTextBox";
            this.mFTextBox.ReadOnly = true;
            this.mFTextBox.Size = new System.Drawing.Size(100, 20);
            this.mFTextBox.TabIndex = 4;
            // 
            // valueCapTextBox
            // 
            this.valueCapTextBox.Location = new System.Drawing.Point(29, 77);
            this.valueCapTextBox.Name = "valueCapTextBox";
            this.valueCapTextBox.Size = new System.Drawing.Size(61, 20);
            this.valueCapTextBox.TabIndex = 3;
            this.valueCapTextBox.TextChanged += new System.EventHandler(this.valueCapTextBox_TextChanged);
            // 
            // filtersTabPage
            // 
            this.filtersTabPage.Controls.Add(this.freqFilterHelpLabel);
            this.filtersTabPage.Controls.Add(this.freqTextBox);
            this.filtersTabPage.Controls.Add(this.freqCapacitorTextBox);
            this.filtersTabPage.Controls.Add(this.freqLabel);
            this.filtersTabPage.Controls.Add(this.capacitorLabel);
            this.filtersTabPage.Controls.Add(this.resistorLable);
            this.filtersTabPage.Controls.Add(this.freqResistorTextBox);
            this.filtersTabPage.Controls.Add(this.freqCapacitorRadioButton);
            this.filtersTabPage.Controls.Add(this.freqResistorRadioButton);
            this.filtersTabPage.Controls.Add(this.freqRadioButton);
            this.filtersTabPage.Location = new System.Drawing.Point(4, 22);
            this.filtersTabPage.Name = "filtersTabPage";
            this.filtersTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.filtersTabPage.Size = new System.Drawing.Size(348, 375);
            this.filtersTabPage.TabIndex = 2;
            this.filtersTabPage.Text = "RC-Фильтры";
            this.filtersTabPage.UseVisualStyleBackColor = true;
            // 
            // freqFilterHelpLabel
            // 
            this.freqFilterHelpLabel.AutoSize = true;
            this.freqFilterHelpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.freqFilterHelpLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.freqFilterHelpLabel.Location = new System.Drawing.Point(317, 346);
            this.freqFilterHelpLabel.Name = "freqFilterHelpLabel";
            this.freqFilterHelpLabel.Size = new System.Drawing.Size(25, 26);
            this.freqFilterHelpLabel.TabIndex = 16;
            this.freqFilterHelpLabel.Text = "?";
            // 
            // freqTextBox
            // 
            this.freqTextBox.Location = new System.Drawing.Point(118, 178);
            this.freqTextBox.Name = "freqTextBox";
            this.freqTextBox.Size = new System.Drawing.Size(100, 20);
            this.freqTextBox.TabIndex = 8;
            this.freqTextBox.TextChanged += new System.EventHandler(this.freqTextBox_TextChanged);
            // 
            // freqCapacitorTextBox
            // 
            this.freqCapacitorTextBox.Location = new System.Drawing.Point(118, 145);
            this.freqCapacitorTextBox.Name = "freqCapacitorTextBox";
            this.freqCapacitorTextBox.Size = new System.Drawing.Size(100, 20);
            this.freqCapacitorTextBox.TabIndex = 7;
            this.freqCapacitorTextBox.TextChanged += new System.EventHandler(this.freqCapacitorTextBox_TextChanged);
            // 
            // freqLabel
            // 
            this.freqLabel.AutoSize = true;
            this.freqLabel.Location = new System.Drawing.Point(19, 181);
            this.freqLabel.Name = "freqLabel";
            this.freqLabel.Size = new System.Drawing.Size(70, 13);
            this.freqLabel.TabIndex = 6;
            this.freqLabel.Text = "Частота (Гц)";
            // 
            // capacitorLabel
            // 
            this.capacitorLabel.AutoSize = true;
            this.capacitorLabel.Location = new System.Drawing.Point(19, 148);
            this.capacitorLabel.Name = "capacitorLabel";
            this.capacitorLabel.Size = new System.Drawing.Size(93, 13);
            this.capacitorLabel.TabIndex = 5;
            this.capacitorLabel.Text = "Конденсатор (Ф)";
            // 
            // resistorLable
            // 
            this.resistorLable.AutoSize = true;
            this.resistorLable.Location = new System.Drawing.Point(19, 117);
            this.resistorLable.Name = "resistorLable";
            this.resistorLable.Size = new System.Drawing.Size(80, 13);
            this.resistorLable.TabIndex = 4;
            this.resistorLable.Text = "Резистор (Ом)";
            // 
            // freqResistorTextBox
            // 
            this.freqResistorTextBox.Location = new System.Drawing.Point(118, 114);
            this.freqResistorTextBox.Name = "freqResistorTextBox";
            this.freqResistorTextBox.Size = new System.Drawing.Size(100, 20);
            this.freqResistorTextBox.TabIndex = 3;
            this.freqResistorTextBox.TextChanged += new System.EventHandler(this.freqResistorTextBox_TextChanged);
            // 
            // freqCapacitorRadioButton
            // 
            this.freqCapacitorRadioButton.AutoSize = true;
            this.freqCapacitorRadioButton.Location = new System.Drawing.Point(22, 71);
            this.freqCapacitorRadioButton.Name = "freqCapacitorRadioButton";
            this.freqCapacitorRadioButton.Size = new System.Drawing.Size(212, 17);
            this.freqCapacitorRadioButton.TabIndex = 2;
            this.freqCapacitorRadioButton.TabStop = true;
            this.freqCapacitorRadioButton.Text = "Конденсатор по частоте и резистору";
            this.freqCapacitorRadioButton.UseVisualStyleBackColor = true;
            // 
            // freqResistorRadioButton
            // 
            this.freqResistorRadioButton.AutoSize = true;
            this.freqResistorRadioButton.Location = new System.Drawing.Point(22, 48);
            this.freqResistorRadioButton.Name = "freqResistorRadioButton";
            this.freqResistorRadioButton.Size = new System.Drawing.Size(212, 17);
            this.freqResistorRadioButton.TabIndex = 1;
            this.freqResistorRadioButton.TabStop = true;
            this.freqResistorRadioButton.Text = "Резистор по частоте и конденсатору";
            this.freqResistorRadioButton.UseVisualStyleBackColor = true;
            // 
            // freqRadioButton
            // 
            this.freqRadioButton.AutoSize = true;
            this.freqRadioButton.Checked = true;
            this.freqRadioButton.Location = new System.Drawing.Point(22, 25);
            this.freqRadioButton.Name = "freqRadioButton";
            this.freqRadioButton.Size = new System.Drawing.Size(100, 17);
            this.freqRadioButton.TabIndex = 0;
            this.freqRadioButton.TabStop = true;
            this.freqRadioButton.Text = "Частота среза";
            this.freqRadioButton.UseVisualStyleBackColor = true;
            // 
            // resistorLedTabPage
            // 
            this.resistorLedTabPage.Controls.Add(this.ledResistorHelpLabel);
            this.resistorLedTabPage.Controls.Add(this.ledResultTextBox);
            this.resistorLedTabPage.Controls.Add(this.currentLabel);
            this.resistorLedTabPage.Controls.Add(this.voltageLabel);
            this.resistorLedTabPage.Controls.Add(this.sourceLabel);
            this.resistorLedTabPage.Controls.Add(this.ledCurrentTextBox);
            this.resistorLedTabPage.Controls.Add(this.ledNominalVoltageTextBox);
            this.resistorLedTabPage.Controls.Add(this.ledSourceVoltageTextBox);
            this.resistorLedTabPage.Location = new System.Drawing.Point(4, 22);
            this.resistorLedTabPage.Name = "resistorLedTabPage";
            this.resistorLedTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.resistorLedTabPage.Size = new System.Drawing.Size(348, 375);
            this.resistorLedTabPage.TabIndex = 3;
            this.resistorLedTabPage.Text = "Резистор для LED";
            this.resistorLedTabPage.UseVisualStyleBackColor = true;
            // 
            // ledResistorHelpLabel
            // 
            this.ledResistorHelpLabel.AutoSize = true;
            this.ledResistorHelpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ledResistorHelpLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ledResistorHelpLabel.Location = new System.Drawing.Point(317, 346);
            this.ledResistorHelpLabel.Name = "ledResistorHelpLabel";
            this.ledResistorHelpLabel.Size = new System.Drawing.Size(25, 26);
            this.ledResistorHelpLabel.TabIndex = 15;
            this.ledResistorHelpLabel.Text = "?";
            // 
            // ledResultTextBox
            // 
            this.ledResultTextBox.Location = new System.Drawing.Point(233, 75);
            this.ledResultTextBox.Name = "ledResultTextBox";
            this.ledResultTextBox.ReadOnly = true;
            this.ledResultTextBox.Size = new System.Drawing.Size(100, 20);
            this.ledResultTextBox.TabIndex = 6;
            // 
            // currentLabel
            // 
            this.currentLabel.AutoSize = true;
            this.currentLabel.Location = new System.Drawing.Point(15, 115);
            this.currentLabel.Name = "currentLabel";
            this.currentLabel.Size = new System.Drawing.Size(88, 13);
            this.currentLabel.TabIndex = 5;
            this.currentLabel.Text = "I светодиода (А)";
            // 
            // voltageLabel
            // 
            this.voltageLabel.AutoSize = true;
            this.voltageLabel.Location = new System.Drawing.Point(15, 78);
            this.voltageLabel.Name = "voltageLabel";
            this.voltageLabel.Size = new System.Drawing.Size(93, 13);
            this.voltageLabel.TabIndex = 4;
            this.voltageLabel.Text = "U светодиода (В)";
            // 
            // sourceLabel
            // 
            this.sourceLabel.AutoSize = true;
            this.sourceLabel.Location = new System.Drawing.Point(15, 43);
            this.sourceLabel.Name = "sourceLabel";
            this.sourceLabel.Size = new System.Drawing.Size(86, 13);
            this.sourceLabel.TabIndex = 3;
            this.sourceLabel.Text = "U источника (В)";
            // 
            // ledCurrentTextBox
            // 
            this.ledCurrentTextBox.Location = new System.Drawing.Point(111, 112);
            this.ledCurrentTextBox.Name = "ledCurrentTextBox";
            this.ledCurrentTextBox.Size = new System.Drawing.Size(100, 20);
            this.ledCurrentTextBox.TabIndex = 2;
            this.ledCurrentTextBox.TextChanged += new System.EventHandler(this.ledCurrentTextBox_TextChanged);
            // 
            // ledNominalVoltageTextBox
            // 
            this.ledNominalVoltageTextBox.Location = new System.Drawing.Point(111, 75);
            this.ledNominalVoltageTextBox.Name = "ledNominalVoltageTextBox";
            this.ledNominalVoltageTextBox.Size = new System.Drawing.Size(100, 20);
            this.ledNominalVoltageTextBox.TabIndex = 1;
            this.ledNominalVoltageTextBox.TextChanged += new System.EventHandler(this.ledNominalVoltageTextBox_TextChanged);
            // 
            // ledSourceVoltageTextBox
            // 
            this.ledSourceVoltageTextBox.Location = new System.Drawing.Point(111, 40);
            this.ledSourceVoltageTextBox.Name = "ledSourceVoltageTextBox";
            this.ledSourceVoltageTextBox.Size = new System.Drawing.Size(100, 20);
            this.ledSourceVoltageTextBox.TabIndex = 0;
            this.ledSourceVoltageTextBox.TextChanged += new System.EventHandler(this.ledSourceVoltageTextBox_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 415);
            this.Controls.Add(this.itemTabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "RadioelectronicsAssistant";
            this.itemTabs.ResumeLayout(false);
            this.resistorPage.ResumeLayout(false);
            this.resistorPage.PerformLayout();
            this.capacitorsPage.ResumeLayout(false);
            this.capacitorsPage.PerformLayout();
            this.filtersTabPage.ResumeLayout(false);
            this.filtersTabPage.PerformLayout();
            this.resistorLedTabPage.ResumeLayout(false);
            this.resistorLedTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl itemTabs;
        private System.Windows.Forms.TabPage resistorPage;
        private System.Windows.Forms.TabPage capacitorsPage;
        private System.Windows.Forms.TabPage filtersTabPage;
        private System.Windows.Forms.ComboBox factorStripComboBox;
        private System.Windows.Forms.ComboBox thirdStripComboBox;
        private System.Windows.Forms.ComboBox secondStripComboBox;
        private System.Windows.Forms.ComboBox firstStripComboBox;
        private System.Windows.Forms.ComboBox errorComboBox;
        private System.Windows.Forms.RadioButton fourRadioButton;
        private System.Windows.Forms.RadioButton threeRadioButton;
        private System.Windows.Forms.RadioButton textRadioButton;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.TextBox errorTextBox;
        private System.Windows.Forms.TextBox valueCapTextBox;
        private System.Windows.Forms.TextBox mFTextBox;
        private System.Windows.Forms.TextBox pFTextBox;
        private System.Windows.Forms.TextBox nFTextBox;
        private System.Windows.Forms.TabPage resistorLedTabPage;
        private System.Windows.Forms.RadioButton freqRadioButton;
        private System.Windows.Forms.RadioButton freqResistorRadioButton;
        private System.Windows.Forms.RadioButton freqCapacitorRadioButton;
        private System.Windows.Forms.Label resistorLable;
        private System.Windows.Forms.TextBox freqResistorTextBox;
        private System.Windows.Forms.Label capacitorLabel;
        private System.Windows.Forms.TextBox freqTextBox;
        private System.Windows.Forms.TextBox freqCapacitorTextBox;
        private System.Windows.Forms.Label freqLabel;
        private System.Windows.Forms.TextBox ledSourceVoltageTextBox;
        private System.Windows.Forms.TextBox ledNominalVoltageTextBox;
        private System.Windows.Forms.TextBox ledCurrentTextBox;
        private System.Windows.Forms.Label sourceLabel;
        private System.Windows.Forms.Label voltageLabel;
        private System.Windows.Forms.Label currentLabel;
        private System.Windows.Forms.TextBox ledResultTextBox;
        private System.Windows.Forms.TextBox resistorPreTextBox;
        private System.Windows.Forms.TextBox resistorFactorLetterTextBox;
        private System.Windows.Forms.TextBox resistorAfterPointTextBox;
        private System.Windows.Forms.TextBox resistorErrorTextBox;
        private System.Windows.Forms.Label resistorHelpLabel;
        private System.Windows.Forms.Label capacitorHelpLabel;
        private System.Windows.Forms.Label ledResistorHelpLabel;
        private System.Windows.Forms.Label freqFilterHelpLabel;
    }
}

