﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkingSigns
{
    class HelpProvider
    {
        static ToolTip helpTooltip;
        private HelpProvider() {}

        public static string GetResistorHelpString()
        {
            SetToolTipTitle("Маркировка резисторов");

            return
                "Резисторы маркируются несколькими цветными полосами, первые отвечают за значение\n" +
                "номинала (то есть сопротивление во сколько Ом имеет резистор), а последняя отвечает\n" +
                "за погрешность.\n\n" +
                "Здесь под 3 и 4 полосы имеется ввиду количество полос, отвечающих за номинал.\n\n" +
                "Обычно первая полоса номинала стоит немного ближе к краю корпуса, чем полоса\n" +
                "погрешности.\n\n" +
                "Также резисторы могут маркироваться текстовым значением (кодовая маркировка).\n" +
                "Формат: <Число><Буква><Число><Буква>. Если какое-то из чисел отсутствует,\n" +
                "пишите 0 в соответствующее поле";
        }

        public static string GetCapacitorHelpString()
        {
            SetToolTipTitle("Маркировка конденсаторов");
            return
                "Чаще всего керамические конденсаторы маркируются тремя цифрами: '103', '333'.\n" +
                "Введите трёхзначное число в поле, чтобы получить номинал такого конденсатора.\n\n";
        }

        public static string GetLedResistorHelp()
        {
            SetToolTipTitle("Токоограничивающий резистор");
            return
                "Чтобы избежать преждевременного выхода из строя, светодиоды подключаются через\n" +
                "токоограничивающий резистор. Его номинал вычисляется исходя из параметров источника\n" +
                "питания и самого светодиода.";
        }

        public static string GetFrequencyFilterHelp()
        {
            SetToolTipTitle("Частотные фильтры");
            return
                "Простейший частотный фильтр представляет из себя кобинацию резистора и конденсатора.\n" +
                "Фильтр высоких частот пропускает только высокие частоты, низких частот только низкие.\n" +
                "Фильтр средних частот представляет из себя комбинацию двух упомянутых.\n\n" +
                "Частота среза фильтра (с какой частоты начинается фильтрация) зависит от номиналов\n" + 
                "компонентов.\n\n" +
                "На этой вкладке можно вычислить частоту среза по номиналам или подобрать детали для\n" +
                "фильтра определенной частоты";
        }

        public static ToolTip Init()
        {
            if (helpTooltip == null)
            {
                helpTooltip = new ToolTip();
            }

            helpTooltip.ToolTipIcon = ToolTipIcon.Info;
            helpTooltip.UseAnimation = true;
            helpTooltip.UseFading = true;
            helpTooltip.IsBalloon = true;
            helpTooltip.AutoPopDelay = 30000;

            return helpTooltip;
        }

        private static void SetToolTipTitle(String title)
        {
            helpTooltip.ToolTipTitle = title;
        }
    }
}
