﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkingSigns
{
    class Resistor
    {
        static int first = 0, second = 0, third = 0;
        static double factor = 0;

        public static void CreateResistorValueControl(ComboBox target, Dictionary<int, string> valDict)
        {
            target.DisplayMember = "Value";
            target.ValueMember = "Key";
            target.DataSource = new BindingSource(valDict, null);
        }

        public static void CreateResistorFloatControl(ComboBox target, Dictionary<float, string> floatDict)
        {
            target.DisplayMember = "Value";
            target.ValueMember = "Key";
            target.DataSource = new BindingSource(floatDict, null);
        }

        private static bool CheckResistorValues(object firstValue, object secondValue,
            object thirdValue, object factorValue, bool isFourStrips)
        {
            if (!isFourStrips)
            {
                if (secondValue != null && thirdValue != null && factorValue != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (firstValue != null && secondValue != null && thirdValue != null && factorValue != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static string CountResistorResult(object firstStrip, object secondStrip, object thirdStrip, object factorStrip, bool isFourStrips)
        {
            if(!CheckResistorValues(firstStrip, secondStrip, thirdStrip, factorStrip, isFourStrips))
            {
                return "0";
            }

            if(!isFourStrips)
            {
                Int32.TryParse(secondStrip.ToString(), out second);
                Int32.TryParse(thirdStrip.ToString(), out third);
                double.TryParse(factorStrip.ToString(), out factor);

                return ((second * 10 + third) * factor).ToString("F99").TrimEnd("0".ToCharArray());
            }
            else
            {
                Int32.TryParse(firstStrip.ToString(), out first);
                Int32.TryParse(secondStrip.ToString(), out second);
                Int32.TryParse(thirdStrip.ToString(), out third);
                double.TryParse(factorStrip.ToString(), out factor);

                return ((first * 100 + second * 10 + third) * factor).ToString("F99").TrimEnd("0".ToCharArray());
            }
        }

        private static double GetFactorByLetter(string factorLetter)
        {
            switch (factorLetter)
            {
                case "R": return 1;
                case "E": return 1;
                case "K": return 1000;
                case "M": return 1000000;
                case "G": return 1000000000;
                case "T": return 1000000000000;
                default: return 0;
            }
        }

        public static double GetErrorByLetter(string errorLetter)
        {
            switch (errorLetter)
            {
                case "E": return 0.001;
                case "L": return 0.002;
                case "R": return 0.005;
                case "P": return 0.01;
                case "U": return 0.02;
                case "A": return 0.05;
                case "B": return 0.1;
                case "C": return 0.25;
                case "D": return 0.5;
                case "F": return 1;
                case "G": return 2;
                case "J": return 5;
                case "K": return 10;
                case "M": return 20;
                case "N": return 30;
                default: return 0;
            }
        }

        public static string CountResistorResult(string beforeDotString, string factorLetter, string afterDotString)
        {
            double beforeDot, afterDot;
            double.TryParse(beforeDotString, out beforeDot);
            double.TryParse(afterDotString, out afterDot);
            double result = beforeDot * GetFactorByLetter(factorLetter) +
                afterDot * Math.Pow(10, -afterDot.ToString().Length) * GetFactorByLetter(factorLetter);
            return result.ToString();
        }

        public static string CalculateLedResistor(string source, string voltage, string current)
        {
            if (source != "" && voltage != "" && current != "")
            {
                double sourceVoltage, ledVoltage, ledCurrent;
                double.TryParse(source, out sourceVoltage);
                double.TryParse(voltage, out ledVoltage);
                double.TryParse(current, out ledCurrent);

                if(ledCurrent != 0)
                    return ((sourceVoltage - ledVoltage) / ledCurrent).ToString() + " Ом";
            }
            return "";
        }
    }
}
