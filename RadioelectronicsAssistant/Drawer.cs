﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkingSigns
{
    class Drawer
    {
        static Font comboBoxFont = new Font("Comic Sans", 12);

        public static SolidBrush InvertBrushColor(SolidBrush input)
        {
            Color temp = input.Color;
            Color output = Color.FromArgb(255 - temp.R, 255 - temp.G, 255 - temp.B);
            SolidBrush result = new SolidBrush(output);
            return result;
        }

        public static void DrawItem(ComboBox cb, DrawItemEventArgs e, Dictionary<int, string> valueDict, Dictionary<int, Brush> valueStrip)
        {
            e.Graphics.FillRectangle(valueStrip[e.Index], new Rectangle(e.Bounds.X, e.Bounds.Y, cb.Width, 20));
            e.Graphics.DrawString(valueDict[e.Index], comboBoxFont, Drawer.InvertBrushColor((SolidBrush)valueStrip[e.Index]), new Point(0, e.Bounds.Y));

            if (e.State == DrawItemState.Selected)
            {
                e.Graphics.FillRectangle(Brushes.White, new Rectangle(e.Bounds.X, e.Bounds.Y, cb.Width, 20));
                e.Graphics.DrawString(valueDict[e.Index], comboBoxFont, Brushes.Black, new Point(0, e.Bounds.Y));
            }
        }

        public static void DrawFloatItem(ComboBox cb, DrawItemEventArgs e, Dictionary<int, Brush> colorDict)
        {
            e.Graphics.FillRectangle(colorDict[e.Index], new Rectangle(e.Bounds.X, e.Bounds.Y, cb.Width, 20));
            e.Graphics.DrawString(((KeyValuePair<float, string>)cb.Items[e.Index]).Value,
                comboBoxFont, Drawer.InvertBrushColor((SolidBrush)colorDict[e.Index]), new Point(0, e.Bounds.Y));

            if (e.State == DrawItemState.Selected)
            {
                e.Graphics.FillRectangle(Brushes.White, new Rectangle(e.Bounds.X, e.Bounds.Y, cb.Width, 20));
                e.Graphics.DrawString(((KeyValuePair<float, string>)cb.Items[e.Index]).Value, comboBoxFont, Brushes.Black, new Point(0, e.Bounds.Y));
            }
        }

    }
}
