# README #

## Functions ##

This application is a helper for radiofans and people that are connected with radioelectronics. It allows to get nominals of components by notation convention (e.g by color, by digits, etc.) and perform some calculations (e.g high pass or low pass filters parameters)

*Note:* Russian only is supported in this application now

## Planned ##

- Add English language support 
- Add more functionality:
    - Other marking types
    - Other calculations

##New feature ideas##
Create an issue in case you have suggestions concerning of the functionality

##Screenshots##
![scr1](https://bytebucket.org/eagafonchikov/radioelectronicsassistant/raw/d161cf844d9ee13e9faca984724bef79033f4532/screenshots/Screen1.png) ![scr2](https://bytebucket.org/eagafonchikov/radioelectronicsassistant/raw/d161cf844d9ee13e9faca984724bef79033f4532/screenshots/Screen2.png)

![scr1](https://bytebucket.org/eagafonchikov/radioelectronicsassistant/raw/d161cf844d9ee13e9faca984724bef79033f4532/screenshots/Screen3.png)